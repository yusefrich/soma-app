import React from 'react';
import ScrollContainer from '../../components/Containers/ScrollContainer';
import Text from '../../components/Typography/Text';
import {StyleSheet, View} from 'react-native';
import Brand from '../../components/Brand';
import TextButton from '../../components/Buttons/TextButton';

export default ({navigation}) => (
  <ScrollContainer canGoBack>
    <View style={styles.container}>
      <View style={styles.textCenter}>
        <Brand width={269.82} height={42.33} />
      </View>
      <Text style={styles.textCenter}>Versão 0.0.1</Text>
      <View style={styles.textCenter}>
        <Text>{new Date().getFullYear()}</Text>
        <Text>Todos os direitos reservados</Text>
      </View>
      <Text style={styles.textCenter}>
        O SOMA é uma plataforma facilitadora de negócios. Ele surgiu em 2019 com
        o propósito de criar uma comunidade para agregar positivamente o dia a
        dia do Empresário.
      </Text>
      <Text style={styles.textCenter}>
        A plataforma reuni lideranças (empreendedores, intraempreendedores e
        sucessores familiares), com o objetivo de oferecer um importante espaço
        de networking com conteúdo e experiência, fomentando o diálogo para
        geração de negócios.
      </Text>
      <Text style={styles.textCenter}>
        Uma das ferramentas para gerar networking é o calendário exclusivo de
        eventos aos filiados, são promovidos aproximadamente 24 eventos em 3
        modalidades, conteúdo, relacionamento e viagens.{' '}
      </Text>
      <Text style={styles.textCenter}>
        Criando ambientes favoráveis ao compartilhamento e relacionamento para
        geração de negócios. Faça parte dessa plataforma e esteja por dentro de
        todo conteúdo, eventos, fóruns de discussões para incentivar a interação
        entre filiados e também impulsionar como os empresários podem se ajudar.
      </Text>
    </View>
    <View style={styles.footerTextContainer}>
      <TextButton
        underline
        onPress={() => navigation.navigate('TermsAndConditionsScreen')}
        text="Termos e Condições de Uso"
      />
    </View>
  </ScrollContainer>
);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  textCenter: {textAlign: 'center', alignItems: 'center', paddingVertical: 10},
  footerTextContainer: {
    flex: 1,
    flexDirection: 'row',
    // flexGrow: 2,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginVertical: 50,
  },
});
