import React from 'react';
import MenuIcon from '../../assets/imgs/menu.svg';
import {useSelector} from 'react-redux';

export default ({container}) => {
  const {theme} = useSelector(state => state.appConfig);

  return <MenuIcon />;
};
